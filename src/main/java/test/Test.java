package test;

//import java.util.List;

import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.Persistence;
//import javax.persistence.Query;

//import dao.DAOActeur;
//import dao.DAOFilm;
import model.Acteur;
import model.Agent;
import model.Film;
import model.Producteur;
import model.Type;
import util.Context;

public class Test {

	public static void main(String[] args) {
//		Acteur a = new Acteur("DeNiro", "Robert", 1.85);
//		insert(a);
//		Acteur b = new Acteur("Thurman", "Uma", 1.8);
//		insert(b);
//		selectById(1);
//		Acteur a = selectById(3);
//		a.setPrenom("Bob");
//		a.setTaille(1.76);
//		update(a);
//		delete(a);
//		selectAll();
		
//		DAOActeur dao = new DAOActeur();
//		System.out.println(dao.selectAll());
		
//		DAOFilm daoF = new DAOFilm();
//		Film f = new Film("Pulp Fiction", 120);
//		daoF.insert(f);
//		Film f2 = new Film("Kill Bill", 135);
//		daoF.insert(f2);
//		System.out.println(dao.selectAll());
//		System.out.println(daoF.selectAll());
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
//		em.find(Film.class, 1);
		System.out.println(em.find(Film.class, 1));
		
		Context.destroy();
//		generateBdd();
	}
	
	public static void generateBdd() {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
		em.getTransaction().begin();
		Producteur p1 = new Producteur("Abid", "Jordan");
		Producteur p2 = new Producteur("Montarou", "Alexis");
		
		Agent a1 = new Agent("Blino", "Yoann");
		Agent a2 = new Agent("Lauricella", "ChrisChris");
		Agent a3 = new Agent("Bouteloup", "Remix");
		
		Acteur ac1 = new Acteur("Lablague", "Toto");
		Acteur ac2 = new Acteur("Staline", "Sylvester");
		Acteur ac3 = new Acteur("Lauricella", "Christopher");
		
		ac1.setAgent(a1);
		ac2.setAgent(a2);
		ac3.setAgent(a3);
		
		Film f1 = new Film("Toto sur un tracteur", 280);
		f1.setType(Type.Comique);
		f1.setProducteur(p2);
		f1.addActeur(ac1);
		f1.addActeur(ac2);
		
		Film f2 = new Film("Toto fait des saltos", 360);
		f2.setType(Type.Horreur);
		f2.setProducteur(p2);
		f2.addActeur(ac1);
		
		Film f3 = new Film("Toto le mentaliste", 30);
		f3.setType(Type.Magie);
		f3.setProducteur(p1);
		f3.addActeur(ac1);
		f3.addActeur(ac2);
		f3.addActeur(ac3);
		
		
		em.persist(f1);
		em.persist(f2);
		em.persist(f3);
		em.getTransaction().commit();
		Context.destroy();
	}
	
}
