package dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Vaisseau;
import util.Context;

public class DAOVaisseau {
	
	public void insert(Vaisseau v) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

        em.getTransaction().begin();
 
        em.persist(v);

        em.getTransaction().commit();
        em.close();
        Context.destroy();
	}
	
	public Vaisseau selectById(int id) {
		
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
        
        Vaisseau v = em.find(Vaisseau.class, id);
        System.out.println(v);
        em.close();
        Context.destroy();
		return v;
	}
	
	public List<Vaisseau> selectAll() {
		
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
        
        Query query =  em.createQuery("from Vaisseau");
        List<Vaisseau> Vaisseaux = query.getResultList();
       
        em.close();
        Context.destroy();
		return Vaisseaux;
	}
	
	public void update(Vaisseau v) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

        em.getTransaction().begin();
        
        em.merge(v);

        //em.persist(a);

        em.getTransaction().commit();
        em.close();
        Context.destroy();
	}
	
	public void delete(Vaisseau v) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();

        em.getTransaction().begin();

        v=em.merge(v);
        em.remove(v);

        //em.persist(p);
   
        em.getTransaction().commit();
        em.close();
        Context.destroy();
	}
}
