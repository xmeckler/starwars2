package model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name="crew")	
public class Equipage {
	
	
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="id", nullable=false)
		private int id;
		
		@Column(name="nom", nullable=false)
		private String nom;
		
		@Column(name="prime")
		private int prime;
		
		@Enumerated(EnumType.STRING)
		private Type type;
		
		//@ManyToMany(mappedBy="mission")
		//private List<Mission> missions = new ArrayList();
		
		//@OneToOne(cascade=CascadeType.ALL)
		//private Vaisseau vaisseau;
		
		//public void addMission(Mission m)
//		{
//			missions.add(m);
//		}
		
		public Equipage(String nom, Type type, int prime) {
		
			this.nom = nom;
			this.type = type;
			this.prime = prime;
							
		
		}
		
		
		
		public Equipage() {
			
		}
		
		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}		
		
		
		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		public int getPrime() {
			return prime;
		}

		public void setPrime(int prime) {
			this.prime = prime;
		}

		public Type getType() {
			return type;
		}

		public void setType(Type type) {
			this.type = type;
		}

		public List<Mission> getMissions() {
			return missions;
		}

		public void setMissions(List<Mission> missions) {
			this.missions = missions;
		}

		public Vaisseau getVaisseau() {
			return vaisseau;
		}

		public void setVaisseau(Vaisseau vaisseau) {
			this.vaisseau = vaisseau;
		}

		@Override
		public String toString() {
			return "Equipage [id=" + id + ", nom=" + nom + ", prime=" + prime + ", type=" + type + "]";
		}
		
}

