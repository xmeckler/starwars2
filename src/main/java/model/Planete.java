package model;

public class Planete {

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name="Planete")
public class Planete {
	
		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		@Column(name="id", nullable=false)
		private int id;
		
		@Column(name="name", nullable=false)
		private String nom;
		
		@Column(name="secteur")
		private String secteur;
		
		@Column(name="langue")
		private String langue;
		
		@Version
		private int version;
		
//		@ManyToOne(mappedBy="Mission")
//		private Mission mission;
		
		public Planete(String nom, String secteur, String langue) {
			this.nom = nom;
			this.secteur = secteur;
			this.langue = langue;
		}

		public Planete() {
			
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		public String getSecteur() {
			return secteur;
		}

		public void setSecteur(String secteur) {
			this.secteur = secteur;
		}

		public String getLangue() {
			return langue;
		}

		public void setLangue(String langue) {
			this.langue = langue;
		}

		public int getVersion() {
			return version;
		}

		public void setVersion(int version) {
			this.version = version;
		}

		@Override
		public String toString() {
			return "Planete [nom=" + nom + ", secteur=" + secteur + ", langue=" + langue + "]";
		}

}
